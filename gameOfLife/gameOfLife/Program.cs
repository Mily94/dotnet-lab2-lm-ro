﻿using System;

namespace gameOfLife {

    class Program {
        static void Main(string[] args) {
            int iteration = 0;
            World world = new World(10, 10, 100);
            Console.WriteLine("iteracja: " + iteration);
            world.printWorld();
            
            while (true) {
                Console.ReadLine();
                iteration++;
                Console.WriteLine("iteracja: " + iteration);
                world.performStep();
                Console.WriteLine();
                world.printWorld();
            }
        }
    }
}