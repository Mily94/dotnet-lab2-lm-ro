﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gameOfLife {

    class World {
        private int[,] world;
        private int sizeX;
        private int sizeY;

        public World(int sizeX, int sizeY, int numberOfStartingCells) {
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            world = new int[sizeX, sizeY];
            fillWorld(numberOfStartingCells);        
        }

        void fillWorld(int numberOfStartingCells) {
            for (int i = 0; i < sizeX; i++) 
                for (int j = 0; j < sizeY; j++) 
                    world[i, j] = 0;

            Random r = new Random();
            for (int i = 0; i < numberOfStartingCells; i++) {
                int column = r.Next(0, sizeX);
                int row = r.Next(0, sizeY);
                world[column, row] = 1;
            }
        }

        public void performStep() {
            int[,] nextWorld = new int[sizeX, sizeY];
       
            for (int i = 0; i < sizeX; i++) {
                for (int j = 0; j < sizeY; j++) {
                    int counter = numberOfNeighbours(i, j);
                    if (isAlive(i,j)) {
                        if (counter < 2)
                            nextWorld[i, j] = 0;
                        if (counter == 2 || counter == 3)
                            nextWorld[i, j] = 1;
                        if (counter > 3)
                            nextWorld[i, j] = 0;
                    } else {
                        if (counter == 3)
                            nextWorld[i, j] = 1;
                    }
                }
            }
            world = nextWorld;
        }

        int numberOfNeighbours(int x, int y) {
            int counter = 0;
            for (int i = x - 1; i < x + 2; i++) 
                for (int j = y - 1; j < y + 2; j++) 
                    if (!(i == x && j == y)) 
                        if (isAlive(i, j))
                            counter++;               
            return counter;
        }

        bool isAlive(int x, int y) {
            if ( x >= 0 && x < sizeX && y >= 0 && y < sizeY)
                return world[x,y] == 1 ? true : false;
            return false;
        }

        public void printWorld() {
            for (int i = 0; i < sizeX; i++) {
                for (int j = 0; j < sizeY; j++) 
                    Console.Write(world[i, j] + " ");
                Console.Write("\n");
            }
        }
    }
}
